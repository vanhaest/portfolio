<?php

/*
* Template Name: Portfolio
* Template Post Type: page
*/

get_header();





?>


<?php
    get_template_part( 'partials/page-header' );
?>


    <!--    Portfolio
        ====================================================================== -->
    <section class="portfolio">
        <div class="container">
            <div class="portfolio-grid">

              <?php
              $args = array(
                'post_type' => 'acme_portfolio',
                //'cat' => '3',
                //'posts_per_page' => 6,
              );
              $loop = new WP_Query( $args );

              while ( $loop->have_posts() ) : $loop->the_post();

                // vars
                // $link = get_permalink();
                $link = get_field('client_website_temp');
                $title = get_field('client_name');
                $text = get_field('client_tagline');
                $image = get_field('header_image');
                ?>
                  <div class="c-portfolio--item js-portfolio--item portfolio--item">
                      <a href="http://<?php echo $link; ?>" target="_blank" class="portfolio--link">
                          <div class="portfolio--item__inner" >
                              <div class="portfolio--title">
                                  <h4><?php echo $title; ?></h4>
                                  <h6><?php echo $text; ?></h6>
                              </div>
                              <img src="<?php echo $image; ?>"
                                   class="portfolio--image portfolio--filter"/>
                          </div>
                      </a>
                  </div>
              <?php endwhile; ?>
            </div>
        </div>
    </section>



<?php
get_footer();
?>