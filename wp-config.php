<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'portfolio');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '9tSYtv3cuXL! }D,]hUVG|>W?j:cqii.uqg/,d;<:=s/39/LlNArqG{xPd-(2c0o');
define('SECURE_AUTH_KEY',  '_3<P{k,FH&s9EV+^CU{~Pin9AK!grYXmP*J0tmGYu>#A59rltyB3FkW{:r/.Pm8`');
define('LOGGED_IN_KEY',    'zqL*F}HZNg948X}E6_^&4[{WyFbt8E&AvUDKaSf*zVxw1U4b/kMXElPV(Xt#Eq{S');
define('NONCE_KEY',        'gMo<kVxVEfIMAB}5GJ?}nJ_nClZ!)d+Uw7 sa+IGV{Q2K+:5;2Y>ii{E: HXcQGl');
define('AUTH_SALT',        'Ci%HSnytlbg0Bq*;4B<f+h{*w_qtV?6=.z{@,)(=<P0ypvSiM)fMxvy&QVb-UxZz');
define('SECURE_AUTH_SALT', '`LyU-sp~Yb:tw<&GU5@,`+Yq~^6Y7CX%n/M^4IOeZPUv/SpU*d`4 G?CmM)Cii#!');
define('LOGGED_IN_SALT',   '{RV/pVx$s=tby2f)rQL%aX:UdsIYZr*@oU(lw|l(UWn8ka&fm+XrUq<A?KX;mR70');
define('NONCE_SALT',       'Y_=]vl6B%]LUL;PZ1CqQ-Y8CdBB@**-%g8i~]-*_#RqWt-n$5[kyeEU=.fsWe`q$');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
